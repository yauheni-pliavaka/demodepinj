package com.example.demo.scheduler;

import org.springframework.stereotype.Component;

@Component
public class FullSync implements SyncTask{
    @Override
    public void runJob() {
        System.out.println("Full sync");
    }
}
