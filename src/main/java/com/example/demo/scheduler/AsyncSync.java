package com.example.demo.scheduler;

import org.springframework.stereotype.Component;

@Component
public class AsyncSync implements SyncTask{
    @Override
    public void runJob() {
        System.out.println("Async run");
    }
}
