package com.example.demo.scheduler;

public interface SyncTask {

    public void runJob();
}
