package com.example.demo.scheduler;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SchedulerService {

    private final SyncTask syncTask;

    public SchedulerService(@Qualifier("fullSync") SyncTask syncTask) {
        this.syncTask = syncTask;
    }

    public void runSyncJob() {
        syncTask.runJob();
    }
}
