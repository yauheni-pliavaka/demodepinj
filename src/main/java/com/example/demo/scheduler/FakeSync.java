package com.example.demo.scheduler;

import org.springframework.stereotype.Component;

@Component
public class FakeSync implements SyncTask{
    @Override
    public void runJob() {
        System.out.println("Fake run");
    }
}
