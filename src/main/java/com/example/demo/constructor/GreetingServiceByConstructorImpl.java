package com.example.demo.constructor;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class GreetingServiceByConstructorImpl implements GreetingServiceByConstructor {

    @Autowired
    private HelloBean4 helloBean4;

    @Override
    public String greeting() {
        return "Hello";
    }
}
