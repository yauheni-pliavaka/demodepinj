package com.example.demo.constructor;

public interface GreetingServiceByConstructor {

    String greeting();
}
