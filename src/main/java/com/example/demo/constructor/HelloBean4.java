package com.example.demo.constructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelloBean4 {

    @Autowired
    private HelloBean3 helloBean3;
}
