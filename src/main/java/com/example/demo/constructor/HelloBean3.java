package com.example.demo.constructor;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class HelloBean3 {

    @Autowired
    private GreetingServiceByConstructor greetingService;

    public String sayHello() {
        return greetingService.greeting();
    }
}
