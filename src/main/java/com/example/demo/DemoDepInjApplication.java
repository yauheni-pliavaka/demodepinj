package com.example.demo;

import com.example.demo.bean.HelloBean;
import com.example.demo.scheduler.SchedulerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoDepInjApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(DemoDepInjApplication.class, args);
        SchedulerService bean = ctx.getBean(SchedulerService.class);
        bean.runSyncJob();
    }

}
