package com.example.demo.setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelloBean2 {

    private GreetingServiceBySetter greetingService;

    @Autowired
    public void setGreetingService(GreetingServiceBySetter greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHello() {
        return greetingService.greeting();
    }
}
