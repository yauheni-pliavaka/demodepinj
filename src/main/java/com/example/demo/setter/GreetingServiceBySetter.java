package com.example.demo.setter;

public interface GreetingServiceBySetter {

    String greeting();
}
