package com.example.demo.setter;

import org.springframework.stereotype.Service;

@Service
public class GreetingServiceBySetterImpl implements GreetingServiceBySetter {

    @Override
    public String greeting() {
        return "Hello";
    }
}
