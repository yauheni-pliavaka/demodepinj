package com.example.demo.bean;

import com.example.demo.constructor.GreetingServiceByConstructor;
import com.example.demo.setter.GreetingServiceBySetter;

public abstract class AbstractA {

    private final GreetingServiceBySetter service;
    private final GreetingServiceByConstructor greetingServiceByConstructor;

    public AbstractA(GreetingServiceBySetter service, GreetingServiceByConstructor greetingServiceByConstructor) {
        this.service = service;
        this.greetingServiceByConstructor = greetingServiceByConstructor;
    }

    private void foo() {
        greetingServiceByConstructor.greeting();
    }
}
