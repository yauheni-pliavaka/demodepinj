package com.example.demo.bean;

import com.example.demo.constructor.GreetingServiceByConstructor;
import com.example.demo.constructor.HelloBean3;
import com.example.demo.setter.GreetingServiceBySetter;
import com.example.demo.setter.HelloBean2;
import org.springframework.stereotype.Component;

@Component
public class A extends ParrentA{

    private final HelloBean helloBean;

    public A(GreetingServiceBySetter service, HelloBean2 helloBean2, HelloBean3 helloBean3, HelloBean helloBean, GreetingServiceByConstructor greetingServiceByConstructor) {
        super(service, helloBean2, helloBean3, greetingServiceByConstructor);
        this.helloBean = helloBean;
    }
}
