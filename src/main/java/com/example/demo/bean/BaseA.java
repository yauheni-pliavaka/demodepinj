package com.example.demo.bean;

import com.example.demo.constructor.GreetingServiceByConstructor;
import com.example.demo.setter.GreetingServiceBySetter;
import com.example.demo.setter.HelloBean2;

public class BaseA extends AbstractA{

    private final HelloBean2 helloBean2;

    public BaseA(GreetingServiceBySetter service, HelloBean2 helloBean2, GreetingServiceByConstructor greetingServiceByConstructor) {
        super(service, greetingServiceByConstructor);
        this.helloBean2 = helloBean2;
    }
}
