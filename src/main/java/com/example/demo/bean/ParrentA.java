package com.example.demo.bean;

import com.example.demo.constructor.GreetingServiceByConstructor;
import com.example.demo.constructor.HelloBean3;
import com.example.demo.setter.GreetingServiceBySetter;
import com.example.demo.setter.HelloBean2;

public class ParrentA extends BaseA{

    private final HelloBean3 helloBean3;

    public ParrentA(GreetingServiceBySetter service, HelloBean2 helloBean2, HelloBean3 helloBean3, GreetingServiceByConstructor greetingServiceByConstructor) {
        super(service, helloBean2, greetingServiceByConstructor);
        this.helloBean3 = helloBean3;
    }
}
