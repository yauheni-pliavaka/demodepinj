package com.example.demo.setter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloBean2Test {

    HelloBean2 bean2;

    @BeforeEach
    void setUp() {
        bean2 = new HelloBean2();
        bean2.setGreetingService(new GreetingServiceBySetterImpl());
    }

    @Test
    void sayHello() {
        String result = bean2.sayHello();
        System.out.println(result);
        assert "Hello".equals(result);
    }
}