package com.example.demo.bean;

import com.example.demo.property.GreetingService;

public class GreetingServiceDammy implements GreetingService {

    @Override
    public String greeting() {
        return "test";
    }
}
