package com.example.demo.bean;

import com.example.demo.property.GreetingService;
import com.example.demo.property.GreetingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class HelloBeanTest {

    HelloBean helloBean;

    @BeforeEach
    void setUp() {
        helloBean = new HelloBean();
        helloBean.greetingService = new GreetingServiceDammy();
    }

    @Test
    void sayHello() {
        String result = helloBean.sayHello();
        System.out.println(result);
        assert "test".equals(result);
    }
}